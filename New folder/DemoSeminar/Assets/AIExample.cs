using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIExample : MonoBehaviour
{
    public NavMeshAgent agent;  //set agent to find path
    public Transform target;    //set target of finding

    // Start is called before the first frame update
    void Start()
    {
        //Define them
        target = GameObject.FindGameObjectWithTag("Target").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //Set position for agent going to
        agent.SetDestination(target.position);
    }
}
